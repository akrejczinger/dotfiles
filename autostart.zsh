#!/bin/zsh
set -ex

# set up modified colemak layout
if [ -s ~/.Xmodmap ]; then
    setxkbmap us -variant colemak
    xmodmap ~/.Xmodmap
fi

# fix caps lock backspace repeat
xmodmap -e "clear Lock"

# after 300ms, repeat the key 70 times per sec?
xset r rate 200 70

# Prevent screen turning off when idle
# sleep 1; xset s off -dpms

# configuration
xrdb -merge ~/.Xresources

# Notify the user that we are done
os_type=`cat /etc/os-release | grep -P '^ID=\w+$' | cut -d"=" -f2`
if [ $os_type = "ubuntu" ]; then
    notify-send "Startup script" "Colemak keyboard layout ready" -t 3000
else
    echo 'shell_notify("Startup script", "Colemak keyboard layout ready")' | awesome-client
fi
