#!/bin/sh
# Custom LF file preview script

HIGHLIGHT_TABWIDTH=4
HIGHLIGHT_STYLE='pablo'
PYGMENTIZE_STYLE='paraiso-dark'

if [[ "$( tput colors )" -ge 256 ]]; then
    pygmentize_format='terminal256'
    highlight_format='xterm256'
else
    pygmentize_format='terminal'
    highlight_format='ansi'
fi

case "$1" in
    *.tar*) tar tf "$1";;
    *.zip) unzip -l "$1";;
    *.rar) unrar l "$1";;
    *.7z) 7z l "$1";;
    *.pdf) pdftotext "$1" -;;
    *) head -n 500 "$1" | \
        pygmentize -f "${pygmentize_format}" -O "style=${PYGMENTIZE_STYLE}";;
    # *) highlight --replace-tabs="${HIGHLIGHT_TABWIDTH}" --out-format="${highlight_format}" \
    #     --style="${HIGHLIGHT_STYLE}" --force -- "$1" || cat "$1";;
esac
