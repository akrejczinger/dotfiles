-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
-- config.color_scheme = 'Edge Dark (base16)'
config.color_scheme = 'Catppuccin Mocha'
-- config.color_scheme = 'Catppuccin Frappe'
config.adjust_window_size_when_changing_font_size = false
config.font = wezterm.font_with_fallback {'IosevkaTerm Nerd Font', 'JetBrains Mono'}

-- and finally, return the configuration to wezterm
return config
