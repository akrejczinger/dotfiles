#!/bin/sh
# TODO this is for arch linux only?

userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap
sysresources=/etc/X11/xinit/.Xresources
sysmodmap=/etc/X11/xinit/.Xmodmap

set bell-style none &

# enable touchpad taps as left click, two-finger tap as right click
touchpad_name=$(cat ~/.touchpad_name)
xinput set-prop $touchpad_name "libinput Tapping Enabled" 1

# set keyboard layout (hardcoded now)
setxkbmap us -variant colemak

# merge in defaults and keymaps
[ -f $sysresources ] && xrdb -merge $sysresources
[ -f $sysmodmap ] && xmodmap $sysmodmap
[ -f $userresources ] && xrdb -merge $userresources
[ -f $usermodmap ] && xmodmap $usermodmap

# Prevent screen turning off when idle
# sleep 1; xset s off -dpms

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

export PATH="$HOME/.poetry/bin:$PATH"
. "$HOME/.cargo/env"
# export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0 #GWSL
# export PULSE_SERVER=tcp:$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}') #GWSL
# export LIBGL_ALWAYS_INDIRECT=1 #GWSL
