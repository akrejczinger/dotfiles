#!/bin/env zsh

# check dropdown session is running
tmux ls | rg dropdown
if [ $? -ne 0]; then
    # start dropdown session
    ssh-add ~/.ssh/id_rsa{,_personal} && ssh-agent tmux new -d -s dropdown &
fi

mouse_xpos=$(xdotool getmouselocation --shell | rg "^X" | cut -d = -f 2)
echo $mouse_xpos
if [ $mouse_xpos -le 1920 ]; then
    # left monitor - full screen
    x=0
    y=0
    w='100%'
    h='100%'
else
    # right monitor - start 1px from top, end 20px from bottom (don't overlap with taskbar)
    # FIXME 1px gap needed because otherwise positioning is bugged, outside screen to the right
    x=0
    y=1
    w='100%'
    h='-32'
fi
tdrop -mta -x $x -y $y -w $w -h $h -s dropdown alacritty
