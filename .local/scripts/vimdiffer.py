#!/usr/bin/env python
import subprocess
import sys

"""
This script can be used with the svn diff --diff_cmd= option
to use the custom vim executable for diffing files
"""

old = sys.argv[6]
new = sys.argv[7]

print("Press enter to continue")
raw_input()
cmd = "nvim -d {0} {1}".format(old, new)
subprocess.call(cmd, shell=True)
