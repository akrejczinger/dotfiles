# Dotfiles using YADM

This repository is a collection of my dotfiles using https://yadm.io

Installation: `yadm clone <git-url>`.

Any conflicting files will be moved to a git stash.
